-- A project defines one build target
project "TinyXml"
	location ( "build/" .. action )
	kind "StaticLib"
	language "C++"
	includedirs {
		"include",
	}
	files {
		"include/tinyxml/**.h","src/**.cpp"
	}

	configuration { "vs2010 or vs2013"}
		buildoptions { "/wd4996" }
		flags { "NoMinimalRebuild", "FatalWarnings"}
	configuration{}
	
